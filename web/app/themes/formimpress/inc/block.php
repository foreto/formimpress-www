<?php
add_action('acf/init', 'my_acf_init_block_types');
function my_acf_init_block_types()
{

    // Check function exists.
    if (function_exists('acf_register_block_type')) {

        // register a testimonial block.
        acf_register_block_type([
            'name'              => 'quality',
            'title'             => __('Quality'),
            'description'       => __('A custom quality block.'),
            'render_template'   => '/template-blocks/blocks/quality.php',
            'category'          => 'formatting',
            'icon'              => 'embed-photo',
            'keywords'          => ['quality', 'quote'],
            'mode'           => 'edit',
        ]);

        acf_register_block_type([
            'name'              => 'clients',
            'title'             => __('Clients'),
            'description'       => __('A custom clients block.'),
            'render_template'   => '/template-blocks/blocks/clients.php',
            'category'          => 'formatting',
            'icon'              => 'star-empty',
            'keywords'          => ['clients', 'ratings'],
            'mode'           => 'edit',
        ]);

        acf_register_block_type([
            'name'              => 'partners',
            'title'             => __('Partners'),
            'description'       => __('A custom partners block.'),
            'render_template'   => '/template-blocks/blocks/partners.php',
            'category'          => 'formatting',
            'icon'              => 'businessman',
            'keywords'          => ['partners', 'logo'],
            'mode'           => 'edit',
        ]);
    }
}
