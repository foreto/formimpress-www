<footer>
    <div class="container footer-container">
        <div class="row">
            <div class="col-12 col-lg-4 col-xl-6 contact-col">
                <div class="row">
                    <div class="col-xl-6 col-lg-12 col-md-6 col-12">
                        <img class="footer-logo d-none d-lg-block" src="<?= get_stylesheet_directory_uri() ?>/assets/img/Formimpress-logo-white.png" alt="FormImpressLogo" />
                        <div class="contact-blue">
                            <p>+48 58 500 49 92</p>
                            <p>contact@formimpress.com</p>
                        </div>
                        <div class="contact-white">
                            <p>Form Impress Agencja Kreatywna<br />
                                ul. Wassowskiego 12/30<br />
                                80-225 Gdańsk</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-12 col-md-6 second-col">
                        <div class="contact-blue">
                            <p>+48 58 500 49 92</p>
                            <p>contact@formimpress.com</p>
                        </div>
                        <div class="contact-white">
                            <p>Form Impress Creative Agency<br />
                                Rocketspace Suites<br />
                                180 sansome St. (6th floor)<br />
                                San Francisco, CA 94133
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-12 menu-col">
                <h3 class="footer-heading">Menu</h3>
                <div class="footer-menu-wrapper">
                    <ul class="footer-menu">
                        <li class="footer-menu-item">
                            <a href="/">O nas</a>
                        </li>
                        <li class="footer-menu-item">
                            <a href="/">Usługi</a>
                        </li>
                        <li class="footer-menu-item">
                            <a href="/">Blog</a>
                        </li>
                        <li class="footer-menu-item">
                            <a href="/">Pobierz broszurę</a>
                        </li>
                        <li class="footer-menu-item">
                            <a href="/">Usługi</a>
                        </li>
                        <li class="footer-menu-item">
                            <a href="/">Blog</a>
                        </li>
                        <li class="footer-menu-item">
                            <a href="/">O nas</a>
                        </li>
                        <li class="footer-menu-item">
                            <a href="/">Pobierz broszurę</a>
                        </li>
                        <li class="footer-menu-item">
                            <a href="/">Blog</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-4 col-lg-6 col-12 person-col">
                <h3 class="footer-heading">Szybki kotakt</h3>
                <div class="persons-wrapper">
                    <div class="contact-person-wrapper d-flex">
                        <div class="person-foto" style='background-image: url("<?= get_stylesheet_directory_uri() ?>/assets/img/aga-foto.png");'></div>
                        <div class="contact-data">
                            <p class="name">Agnieszka Partyka</p>
                            <p class="role blue">Business Development Manager</p>
                            <p class="phone"><a href="tel:+48514139185">+48 514 139 185</a></p>
                            <p class="phone"><a href="mailto:a.partyka@formimpress.com">a.partyka@formimpress.com</a></p>
                        </div>
                    </div>
                    <div class="contact-person-wrapper d-flex">
                        <div class="person-foto" style='background-image: url("<?= get_stylesheet_directory_uri() ?>/assets/img/aga-foto.png");'></div>
                        <div class="contact-data">
                            <p class="name">Agnieszka Partyka</p>
                            <p class="role blue">Business Development Manager</p>
                            <p class="phone"><a href="tel:+48514139185">+48 514 139 185</a></p>
                            <p class="phone"><a href="mailto:a.partyka@formimpress.com">a.partyka@formimpress.com</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-bar">
            <div class="social-icons">
                <ul>
                    <li><a href="#"><img src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/fb.svg" alt="fb" /></a></li>
                    <li><a href="#"><img src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/instagram.svg" alt="instagram" /></a></li>
                    <li><a href="#"><img src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/linkedin.svg" alt="linkedin" /></a></li>
                    <li><a href="#"><img src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/behance.svg" alt="behance" /></a></li>
                    <li><a href="#"><img src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/dribbble.svg" alt="dribbble" /></a></li>
                </ul>
            </div>
            <div class="rights-info">
                <p>2021. All rights reserved</p>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>

</html>