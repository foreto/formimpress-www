<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FormImpress</title>
    <?php wp_head(); ?>
</head>

<body>

    <header>

        <div class="row header">
            <div class="col-md-3 col-7 header__logo">
                <img src="<?= get_stylesheet_directory_uri() ?>/assets/img/Formimpress-logo.png" alt="FormImpressLogo" />
            </div>
            <div class="col-md-9 col-5 header__wrapper">
                <div class="header__wrapper__content">

                    <div class="button-wrapper">
                        <button class="initial-valuation">Wstępna wycena</button>
                    </div>

                    <div class="social-content">
                        <ul>
                            <li><a href="#"><img src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/fb.svg" alt="fb" /></a></li>
                            <li><a href="#"><img src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/instagram.svg" alt="instagram" /></a></li>
                            <li><a href="#"><img src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/linkedin.svg" alt="linkedin" /></a></li>
                            <li><a href="#"><img src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/behance.svg" alt="behance" /></a></li>
                            <li><a href="#"><img src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/dribbble.svg" alt="dribbble" /></a></li>
                        </ul>
                    </div>
                    <div class="action-buttton">
                        <div class="lang-content">
                            <?php $langs_array = pll_the_languages(['dropdown' => 1, 'hide_current' => 1, 'raw' => 1]); ?>
                            <div class="language-switcher">
                                <div value=""><?= strtoupper(pll_current_language()) ?></div>
                                <?php foreach ($langs_array as $single_lang) : ?>
                                    <div data-value="<?= $single_lang['url'] ?>" class="lang-dropdown" onClick="changeLang(this)"><?= strtoupper($single_lang['slug']) ?></div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="nav-trigger">
                            <div id="menu-dots" class="menu-dots-open">
                                <div class="menu-title">Menu</div>
                            </div>
                        </div>
                    </div>
                    <?php require_once('menu.php') ?>
                </div>
            </div>
        </div>

    </header>