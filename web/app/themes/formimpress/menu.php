<nav class="sidebar-wrapper">
    <aside class="sidebar">
        <div class="row">
            <div class="col">
                <?php
                wp_nav_menu($args = [
                    'menu' => 'Główne menu',
                    'menu_class' => 'main-menu'
                ])
                ?>
            </div>
        </div>
        <div class="row language-mobile">
            <div class="col-12">
                <?php $langs_array = pll_the_languages(array('dropdown' => 1, 'hide_current' => 1, 'raw' => 1)); ?>
                <select class="language-switcher-mobile" onchange="location = jQuery('.language-switcher-mobile').val()">
                    <option value="" disabled selected><?= strtoupper(pll_current_language()) ?></option>
                    <?php foreach ($langs_array as $single_lang) : ?>
                        <option value="<?= $single_lang['url'] ?>"><?= strtoupper($single_lang['slug']) ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col social-container">
                <ul class="soc-icons">
                    <li><a href="#"><img src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/fb.svg" alt="fb" /></a></li>
                    <li><a href="#"><img src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/instagram.svg" alt="instagram" /></a></li>
                    <li><a href="#"><img src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/linkedin.svg" alt="linkedin" /></a></li>
                    <li><a href="#"><img src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/behance.svg" alt="behance" /></a></li>
                    <li><a href="#"><img src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/dribbble.svg" alt="dribbble" /></a></li>
                </ul>
            </div>
        </div>
        <div class="row block-information">
            <div class="col-md-6">
                <p>Form Impress Agencja Kreatywna<br>
                    ul. Wassowskiego 12/30<br>
                    80-225 Gdańsk</p>
            </div>
            <div class="col-md-6">
                <p>+48 58 500 49 92 <br>
                    contact@formimpress.com</p>
            </div>
        </div>
    </aside>
</nav>