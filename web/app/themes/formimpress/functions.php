<?php

function form_impress_scripts()
{
    wp_enqueue_style('bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css', 1);
    wp_enqueue_style('app_css', get_template_directory_uri() . "/dist/style.bundle.css", 1);


    wp_enqueue_script('jquery', '//code.jquery.com/jquery-3.6.0.min.js', wp_get_theme()->get(1));
    wp_enqueue_script('bootstrap_scripts', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', wp_get_theme()->get(1));
    wp_enqueue_script('menuOpen', asset('menuOpen.js', 'js'), wp_get_theme()->get(1));
    wp_enqueue_script('changeLang', asset('changeLang.js', 'js'), wp_get_theme()->get(1));
    wp_enqueue_script('openLangMenu', asset('openLangMenu.js', 'js'), wp_get_theme()->get(1));
    wp_enqueue_script('app_js', get_template_directory_uri() . "/dist/main.bundle.js/", wp_get_theme()->get(1));
}
add_action('wp_enqueue_scripts', 'form_impress_scripts');

if (!function_exists('form_impress_register_nav_menu')) {

    function form_impress_register_nav_menu()
    {
        register_nav_menus(array(
            'primary_menu' => __('Primary Menu', 'text_domain'),
            'footer_menu'  => __('Footer Menu', 'text_domain'),
        ));
    }
    add_action('after_setup_theme', 'form_impress_register_nav_menu', 0);
}

function asset($path, $type)
{
    $url = get_template_directory_uri() . '/assets';

    switch ($type) {
        case 'img':
            $url .= '/img/';
            break;
        case 'icon':
            $url .= '/img/icons/';
            break;
        case 'css':
            $url .= '/css/';
            break;
        case 'js':
            $url .= '/js/';
            break;
    }
    return $url . $path;
}

require_once(__DIR__ . '/inc/block.php');
