<?php

/**
 * Quality Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'quality-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'quality';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}

$title_section = get_field('title_section') ?: '';
$realizations = get_field('realizations') ?: '';
$bottom_link = get_field('bottom_link') ?: '';

?>


<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="container-quality">
        <h2 class="quality-section-heading"><?= $title_section ?></h2>
        <div class="row">
            <?php foreach ($realizations as $simple_realization) : ?>
                <div class="col-12 col-lg-6">
                    <div class="quality-item-wrapper">
                        <div class="<?php echo esc_attr($className); ?>__image" style="background-image: url('<?= $simple_realization['image'] ?>')"></div>
                        <div class="realizations-content-wrapper">
                            <div class="realizations-content">
                                <h4><?= $simple_realization['title_of_realizations'] ?></h4>
                                <p><?= $simple_realization['description'] ?></p>
                                <?php foreach ($simple_realization['buttons'] as $button) : ?>
                                    <a class="realisation-button <?= $button['light_theme'] ? 'light' : '' ?>" href="<?= $button['simple_button']['url'] ?>" title="Change theme button"><?= $button['simple_button']['title'] ?></a>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
        <a class="view-more-realisation" href="<?= $bottom_link['url'] ?>" title="View more">
            <span><?= $bottom_link['title'] ?></span>
            <img class="iiii" src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/chevron-right-orange.svg" alt="go" />
        </a>
    </div>
</div>