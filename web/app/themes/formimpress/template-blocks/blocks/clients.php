<?php

/**
 * Clients Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'clients-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'clients';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}

$section_title = get_field('section_title') ?: '';
$clients = get_field('clients') ?: '';
$bottom_link = get_field('bottom_link') ?: '';

?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="container-clients">
        <h1 class="section-heading"><?= $section_title ?></h1>
        <div class="clients-wrapper">
            <?php foreach ($clients as $client) : ?>
                <div class="single-client">
                    <div class="client-img" style="background-image: url('<?= $client['client_image'] ?? '' ?>');"></div>
                    <blockquote class="<?php echo esc_attr($className); ?>__quote"> <?= $client['client_quote'] ?? '' ?></blockquote>
                    <div class="client-info-wrap">
                        <h6 class="<?php echo esc_attr($className); ?>__name"> <?= $client['client_name'] ?? '' ?></h6>
                        <p class="<?php echo esc_attr($className); ?>__profession"><?= $client['client_profession'] ?? '' ?></p>
                    </div>
                    <img class="quote-icon" src="<?= asset('quote.svg', 'icon') ?>" alt="quote" />
                </div>
            <?php endforeach ?>
        </div>
        <a class="view-more-realisation" href="<?= $bottom_link['url'] ?>" title="View more">
            <span><?= $bottom_link['title'] ?></span>
            <img src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/chevron-right-orange.svg" alt="go" />
        </a>
    </div>
</div>