<?php

/**
 * Partners Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'partners-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'partners';
if (!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}

$title_section = get_field('title_section') ?: '';
$partners_logo = get_field('partners_logo') ?: '';

?>


<div id="<?php echo esc_attr($id); ?>" class="block <?php echo esc_attr($className); ?>">
    <div class="container-partners">
        <div class="row">
            <div class="col-md-3 col-12">
                <h2 class="<?php echo esc_attr($className); ?>__heading"><?= $title_section ?></h2>
            </div>
            <div class="col-md-9 col-12 p-0">
                <div class="<?php echo esc_attr($className); ?>__logos-wrap">
                    <?php foreach ($partners_logo as $logo) : ?>
                        <img class="<?php echo esc_attr($className); ?>__img my-auto mx-auto" src="<?= $logo['single_partner'] ?>" />
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</div>