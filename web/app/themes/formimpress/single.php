<?php
/*
 * Template Name: Single Blog Post
 * Template Post Type: pos
 */
the_post();
?>

<?php require_once("header.php") ?>

<main class="single-blog-post">
    <div class="container container-blog">
        <a href="/" class="back-btn"><img src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/chevron-left-blue.svg" alt="<"></a>
        <div class="post-data-wrapper">
            <h2 class="post-title"><?php the_title(); ?></h2>
            <div class="post-meta d-block d-sm-flex">
                <p><?php the_date('d/m/Y', '', ''); ?></p>
                <p><?php the_author(); ?></p>
                <?php the_category('', '<p>'); ?>
            </div>
        </div>
        <?php the_content(); ?>

        <div class="single-post-links-section">
            <div class="d-md-flex d-grid justify-content-between">
                <div class="prev-post post-link">
                    <?php previous_post_link('%link', '<img class="post-nav-icon" src="' . get_stylesheet_directory_uri() . '/assets/img/icons/chevron-left-blue.svg" alt="<"> %title', 'yes'); ?>
                </div>
                <div class="share-wrapper">
                    <h3>Udostępnij</h3>
                    <div class="d-flex justify-content-between">
                        <a href="#"><img src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/fb.svg" alt="fb" /></a>
                        <a href="#"><img src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/instagram.svg" alt="instagram" /></a>
                        <a href="#"><img src="<?= get_stylesheet_directory_uri() ?>/assets/img/icons/linkedin.svg" alt="linkedin" /></a>
                    </div>
                </div>
                <div class="next-post post-link">
                    <?php next_post_link('%link', '%title <img class="post-nav-icon" src="' . get_stylesheet_directory_uri() . '/assets/img/icons/chevron-right-orange.svg" alt="<">', 'yes'); ?>
                </div>
            </div>
        </div>

    </div>

</main>
<div class="container">
    <div class="single-post-bottom-section">
        <div class="row">
            <div class="col-12 col-md-6 left-col">
                <h3>Masz więcej pytań do artykułu? Odezwij się!</h3>
                <div class="row">
                    <div class="col-3 col-md-12 col-lg-3">
                        <img src="<?= get_stylesheet_directory_uri() ?>/assets/img/realization4.png" alt="" />
                    </div>
                    <div class="col-9 col-md-12 col-lg-9">
                        <div class="contact-data-wrapper">
                            <p class="name">Agnieszka Partyka</p>
                            <p class="role">Business Development Manager</p>
                            <p class="phone">+48 514 139 185</p>
                            <p class="email">a.partyka@formimpress.com</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 right-col">
                <h3>Chcesz dostawać powiadomienia
                    o nowych artykułach? Zapisz się!</h3>
                <div class="input-wrapper">
                    <input type="text">
                    <button>Wyślij</button>
                </div>
                <div class="checkbox-wrapper">
                    <input type="checkbox" id="agreement" name="agreement">
                    <label for="agreement">Wyrażam zgodę na przetwarzanie danych osobowych</label>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once("footer.php") ?>