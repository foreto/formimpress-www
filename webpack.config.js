require('babel-polyfill');
require('whatwg-fetch');

const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
const StylelintPlugin = require('stylelint-webpack-plugin');

module.exports = (env, options) => {
    return {
        entry: [
            'babel-polyfill',
            'whatwg-fetch',
            './web/app/themes/formimpress/assets/entry.js',
        ],
        output: {
            path: path.resolve(__dirname, 'web/app/themes/formimpress/dist'),
            filename: 'main.bundle.js'
        },
        target: 'node',
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader'
                    }
                },
                {
                    test: /\.scss$/,
                    use: [
                        (options.mode == 'production' ? MiniCssExtractPlugin.loader : 'style-loader'),
                        {
                            loader: 'css-loader',
                            options: {
                                url: false,
                            },
                        },
                        'postcss-loader',
                        'sass-loader']
                }
            ]
        },
        plugins: [
            new CleanWebpackPlugin(),
            new MiniCssExtractPlugin({
                filename: 'style.bundle.css',
            }),
            new BrowserSyncPlugin({
                files: "**/*.php",
                proxy: "http://localhost:8080", // your dev server here
            }),
            new ESLintPlugin(),
        ]
    };
}